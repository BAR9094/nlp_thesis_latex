\babel@toc {english}{}\relax 
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {figure}{\numberline {2.1}{\ignorespaces Transformer Architecture as presented in the original paper [Vas+17]. This diagram illustrates the various layers of the architecture within the Encoder and Decoder, demonstrating the flow of data through each component.}}{4}{figure.caption.3}%
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {figure}{\numberline {4.1}{\ignorespaces Number of Fandom Articles included in our Dataset for each Work}}{18}{figure.caption.4}%
\contentsline {figure}{\numberline {4.2}{\ignorespaces List of Individual Novels per Franchise Utilized for Passage Retrieval in the Experiments}}{19}{figure.caption.5}%
\contentsline {figure}{\numberline {4.3}{\ignorespaces SPARQL query to retrieve selected items, fandoms, fandom statements, and character names filtered by the "lotr" fandom from wikidata.}}{20}{figure.caption.6}%
\contentsline {figure}{\numberline {4.4}{\ignorespaces Sample Fandom Article for the Character "Gollum" from "The Lord of the Rings" Fandom Site (\url {https://lotr.fandom.com/wiki/Gollum})}}{21}{figure.caption.7}%
\addvspace {10\p@ }
\contentsline {figure}{\numberline {5.1}{\ignorespaces Structures of All Prompts Used for Zero-Shotting and Passage Retrieval in the Baseline Experiment}}{24}{figure.caption.8}%
\contentsline {figure}{\numberline {5.2}{\ignorespaces Scatterplot of BLEUScores of character descriptions generated with Mixtral7b without ($P^{z}$) and with baseline passage retrieval ($P^{r}$) plotted against each other}}{26}{figure.caption.9}%
\contentsline {figure}{\numberline {5.3}{\ignorespaces Scatterplot of BERTScores of character descriptions generated with Mixtral7b without ($P^{z}$) and with baseline passage retrieval ($P^{r}$) plotted against each other}}{27}{figure.caption.10}%
\contentsline {figure}{\numberline {5.4}{\ignorespaces Sorted differences between BLEUScores of character descriptions generated with Mixtral7b without ($P^{z}$) and with baseline passage retrieval ($P^{r}$) for every prompt ($P_{1}, P_{2}, P_{3}, P_{4}$).}}{28}{figure.caption.11}%
\contentsline {figure}{\numberline {5.5}{\ignorespaces Sorted differences between BERTScores of character descriptions generated with Mixtral7b without ($P^{z}$) and with baseline passage retrieval ($P^{r}$) for every prompt ($P_{1}, P_{2}, P_{3}, P_{4}$).}}{29}{figure.caption.12}%
\contentsline {figure}{\numberline {5.6}{\ignorespaces Boxplots of BLEU- and BERTScores of character descriptions generated with Mixtral7b for every prompt ($P_{1}, P_{2}, P_{3}, P_{4}$) with and without baseline passage retrieval.}}{30}{figure.caption.13}%
\contentsline {figure}{\numberline {5.7}{\ignorespaces T-Test and Spearman's correlation (with Corresponding p-values) of BLEU- and BERTScores of character descriptions generated with Mixtral7b for every prompt ($P_{1}, P_{2}, P_{3}, P_{4}$) with and without baseline passage retrieval.}}{31}{figure.caption.14}%
\contentsline {figure}{\numberline {5.8}{\ignorespaces Scatterplot of BLEUScores of character descriptions generated with Llama3 and Gemma2 on $P_1$ without ($P^{z}$) and with passage embedding retrieval ($P^{r}$) plotted against each other.}}{34}{figure.caption.15}%
\contentsline {figure}{\numberline {5.9}{\ignorespaces Scatterplot of BERTScore of character descriptions generated with Llama3 and Gemma2 on $P_1$ without ($P^{z}$) and with passage embedding retrieval ($P^{r}$) plotted against each other.}}{34}{figure.caption.16}%
\contentsline {figure}{\numberline {5.10}{\ignorespaces T-Test and Spearman's Correlation (with Corresponding p-values) after prompting Llama3 and Gemma2 with $P_{1}$ with and without passage embedding retrieval}}{35}{figure.caption.17}%
\contentsline {figure}{\numberline {5.11}{\ignorespaces Sorted differences between BLEU- and BERTScores of character descriptions generated with Llama3 and Gemma2 of $P_{1}$ without ($P_{1}^{z}$) and with passage embedding retrieval ($P_{1}^{r}$).}}{35}{figure.caption.18}%
\contentsline {figure}{\numberline {5.12}{\ignorespaces Prompting result for the character ``Nymphadora Tonks'' from the Harry Potter franchise generated with $P_{1}^{r}$ (passage embedding retrieval) in Gemma2.}}{36}{figure.caption.19}%
\contentsline {figure}{\numberline {5.13}{\ignorespaces Sorted Differences of BLEU- and BERTScores for $P_{1}$ Between Baseline Retrieval on Mixtral7b and Passage Embedding Retrieval on Llama3 Before and After Retrieval}}{38}{figure.caption.20}%
\contentsline {figure}{\numberline {5.14}{\ignorespaces Sorted Differences of BLEU- and BERTScores for $P_{1}$ Between Baseline Retrieval on Mixtral7b and Passage Embedding Retrieval on Gemma2 Before and After Retrieval}}{39}{figure.caption.21}%
\contentsline {figure}{\numberline {5.15}{\ignorespaces T-Test and Spearman's Correlation (with Corresponding p-values) of BLEU- and BERTScore for $P_{1}$ Using Base Retrieval and Selected Embedded Chunk Retrieval on Llama3 and Gemma2}}{39}{figure.caption.22}%
\contentsline {figure}{\numberline {5.16}{\ignorespaces Distribution of the embedding scores of all passages used for passage retrieval computed with e5-mistral.}}{40}{figure.caption.23}%
\contentsline {figure}{\numberline {5.17}{\ignorespaces Distribution of embedding similarity scores (in percentages) for all passages used for retrieval, categorized by fandom and score interval, calculated using e5-mistral.}}{41}{figure.caption.24}%
\contentsline {figure}{\numberline {5.18}{\ignorespaces T-Test and Spearman's Correlation (with Corresponding p-values) of BLEU and BERTScore for $P_{1}$ Using Mixtral7b Base Retrieval and Embedded Chunk Retrieval on Llama3 and Gemma2 only on Characters with all Selected Embedded Chunks having a Score of 75 or higher }}{42}{figure.caption.25}%
\contentsline {figure}{\numberline {5.19}{\ignorespaces Scatterplot of BLEUScores of character descriptions generated with Llama3 on $P_{1}$ without ($P^{z}$) and with passage embedding retrieval ($P^{r}$) plotted against each other, color-coded and dissected into franchises and surrounded with convex hulls.}}{43}{figure.caption.27}%
\contentsline {figure}{\numberline {5.20}{\ignorespaces Scatterplot of BERTScores of character descriptions generated with Llama3 on $P_{1}$ without ($P^{z}$) and with passage embedding retrieval ($P^{r}$) plotted against each other, color-coded and dissected into franchises and surrounded with convex hulls.}}{44}{figure.caption.29}%
\addvspace {10\p@ }
\addvspace {10\p@ }
\addvspace {10\p@ }
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
