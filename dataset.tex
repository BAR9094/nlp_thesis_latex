
\chapter{Gathering of Literature}
\label{chap:dataset}
Unfortunately, there's barely any open-source collection of literature with
characterizations available. Examples like ``Romeo and Juliet'', ``Moby Dick'', ``Frankenstein'' or ``Alice's Adventures in Wonderland'' are rare cases where enough fandom exists to create accessible and reviewed content. In most other instances, it seems too risky to use public-domain literature, as these collections predominantly consist of less popular books with minimal fanbase and related content. Popular literature, with its larger online presence, results in more detailed and reviewed community-generated content, such as characterizations and summaries, which are valuable as reference points for my generated characterizations, which is why I will mostly rely on non-open source literature for this thesis.\\

During the process of using Wikidata, a free and open knowledge database, to query characters and filter personal descriptions from books, I discovered that many of these descriptions contain references to articles from fandom.com, the world's most popular open-source wiki platform for fan-related content. Initially, I planned to query Wikidata for all characters linked to Fandom articles to gather literature with the most comprehensive fandom articles. However, I realized that not all character descriptions in Wikidata include Fandom article links. Some character descriptions are missing Fandom article URLs, making it insufficient to rely solely on Wikidata for content. Additionally, there are instances of multiple articles linked to one character. Some articles are in different languages, while others are older versions or from different universes within the same saga. In most cases, I was able to choose to use the newest, longest English version but this was not always possible. For example, when fetching Dune character fandom articles, I had to manually sort out some characters. The Dune fandom includes characters from the ``Dune Encyclopedia'' and ``Expanded Dune'', as well as from the original ``Dune'' by Frank Herbert. This overlap made it problematic to compare information about the same character in different contexts, especially when relevant information might not be available across all contexts.\\

In the end, I used multiple methods. First, I queried Wikidata to quickly obtain a large number of characters, then manually deleted duplicates and added additional characters with their corresponding fandom URLs by hand. I found that the fetched articles varied significantly in length, requiring me to cut some of them down so they were roughly the same size. I achieved this by sequentially removing paragraphs from the bottom of each original article until they reached the desired length. Truncating the end of articles yields better results than truncating the beginning, as it is unlikely that the ending contains any essential information about the character that has not been mentioned before.
Since readers of this thesis might not have access to all the non-open-source literature I used, I aimed to minimize the number of sources to make the results easier to replicate and verify. Ultimately, I was able to obtain character data for 570 characters from six franchises in total. All of the books contain text decorations and structural elements such as chapters, sections, and page numbers, which remained present after converting the PDFs and text files and loading them into memory. These elements had to be manually filtered out using regular expressions before further processing. All the results are linked in the appendix. \\

\begin{figure}[H]
  \centering
  \begin{tabular}{|l|c|}
    \hline
    \textbf{Book}                        & \textbf{Amount} \\
    \hline
    Harry Potter                         & 157             \\
    \hline
    Dune                                 & 95             \\
    \hline
    Twilight                             & 71              \\
    \hline
    The Lord of the Rings                & 106              \\
    \hline
    The Hitchhiker's Guide to the Galaxy & 82             \\
    \hline
    The Hunger Games                     & 29              \\
    \hline
    \hline
    Total                                & 570             \\
    \hline
  \end{tabular}

  \caption{Number of Fandom Articles included in our Dataset for each Work}
  \label{fig:fandused}
\end{figure}



\begin{figure}[H]
  \centering
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Franchise}                                    & \textbf{Book}
    \\
    \hline
    \multirow{7}{*}{Harry Potter}                         & The Sorcerers Stone                  \\
                                                          & The Chamber of Secrets               \\
                                                          & The Prisoner of Azkaban              \\
                                                          & The Goblet of Fire                   \\
                                                          & The Order of the Phoenix             \\
                                                          & The Half-Blood Prince                \\
                                                          & The Deathly Hallows                  \\
    \hline
    \multirow{6}{*}{Dune}                                 & Chapterhouse\_ Dune                  \\
                                                          & Children of Dune                     \\
                                                          & Dune                                 \\
                                                          & Dune Messiah                         \\
                                                          & God Emperor of Dune                  \\
                                                          & Heretics of Dune                     \\
    \hline
    \multirow{4}{*}{Twilight}                             & Twilight                             \\
                                                          & New Moon                             \\
                                                          & Eclipse                              \\
                                                          & Breaking Dawn                        \\
    \hline
    \multirow{3}{*}{The Lord of the Rings }               & The Fellowship of the Ring           \\
                                                          & The Two Towers                       \\
                                                          & The Return of the King               \\

    \hline
    \multirow{1}{*}{The Hitchhiker's Guide to the Galaxy} & The Hitchhiker's Guide to the Galaxy \\

    \hline
    \multirow{1}{*}{The Hunger Games }                    & The Hunger Games                     \\

    \hline
  \end{tabular}
  \label{fig:litused}
  \caption{List of Individual Novels per Franchise Utilized for Passage Retrieval in the Experiments}
\end{figure}


\section{Querying Wikidata}

As previously mentioned, Wikidata provides a way to directly retrieve information from its database through HTTP requests. To do so, one must formulate a SPARQL query. SPARQL has emerged as the standard semantic query language for databases that store their data in Resource Description Framework (RDF). It allows for querying required and optional patterns, conjunctions, disjunctions, supports complex queries with nested queries and then can return results in multiple formats including XML, JSON, and CSV, making it highly flexible for integration with different systems. RDF represents data as a directed graph composed of triplet statements: subject, predicate, and object. These triplets are structured by the database designer to reflect the underlying data and their relationships.

To get a more practical understanding, I will now briefly explain a query that I built to retrieve character names with their corresponding fandom URLs given a fandom page name (in this case Lord of the Rings).

\begin{figure}[H]
  \centering
  \begin{lstlisting}[language=SPARQL, frame=single]
 SELECT
 ?item
 ?fandom
 ?fandomStatement
 ?characterName
 WHERE {
 ?item wdt:P31 wd:Q3658341.
 ?item p:P6262 ?fandomStatement.
 ?fandomStatement ps:P6262 ?fandom.
 ?fandomStatement pq:P1810 ?characterName.
 BIND(STRBEFORE(?fandom, ":") AS ?firstHalf).
 FILTER (?firstHalf = "lotr").
 SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
 }
\end{lstlisting}
  \caption{SPARQL query to retrieve selected items, fandoms, fandom statements, and character names filtered by the "lotr" fandom from wikidata.}
\end{figure}
Below the SELECT clause, we define some variables to store parts of the triples specified in the WHERE clause. This query retrieves entities that are instances (P31) of literary characters (Q3658341) and stores the statement node for the Fandom Article ID (P6262) in the \texttt{?fandomStatement} variable. From this statement node, we extract the fandom object (P6262) and store it in the \texttt{?fandom} variable. We also extract the character name (p1810) from the statement node and store it in the \texttt{?characterName} variable. Finally, we filter the results to include only those where the fandom URL starts with the desired prefix "lotr". Note that I used different qualifiers in the query, such as p, ps, or pq, to access different desired parts of the predicate, such as only the statement node, the actual value, or the qualifier value.\cite{w3c_sparql_2008, wikidata_sparql_tutorial}


\subsection{Scraping Fandom Articles}


Nearly every character article from fandom.com starts out with a quote that mostly belongs to that character and describes him well. Then there is a general short summary, that gives the most important aspects of that character followed by an agenda for all the specific content that follows right after. Usually, the agenda contains topics such as ``Biography'', ``Character'' and mostly also ``Gallery'' and ``References''. The agenda is different for every character. Fandom articles do not have to be about characters but can also be about anything else e.g. places, traditions, ethnics, artefacts or ideas in the same universe that are interesting to the writers.
Any article is at least peer-reviewed twice before publishment and anyone after that can review it and ask for changes if they detect any faults. The Figure \ref{fig:fand_gollum} illustrates the structure and appearance of such an article.

\begin{figure}[h]
  \centering
  \croppedimage{40}{600}{10}{40}
  \begin{center}
    \wavyline
  \end{center}
  \croppedimage{40}{210}{10}{560}
  \caption{Sample Fandom Article for the Character "Gollum" from "The Lord of the Rings" Fandom Site (\url{https://lotr.fandom.com/wiki/Gollum})}
  \label{fig:fand_gollum}
\end{figure}