\babel@toc {english}{}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Background}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Tokenization}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}The Transformer}{4}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Encoder}{5}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Decoder}{5}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}BERT}{6}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Embeddings}{6}{subsection.2.3.1}%
\contentsline {section}{\numberline {2.4}Fine-Tuning}{7}{section.2.4}%
\contentsline {section}{\numberline {2.5}Quantization}{7}{section.2.5}%
\contentsline {subsection}{\numberline {2.5.1}BERTScore}{8}{subsection.2.5.1}%
\contentsline {section}{\numberline {2.6}BLEU-Score}{9}{section.2.6}%
\contentsline {section}{\numberline {2.7}RAG}{10}{section.2.7}%
\contentsline {chapter}{\numberline {3}Related Work}{11}{chapter.3}%
\contentsline {section}{\numberline {3.1}Coreference in Long Documents using Hierarchical Entity Merging}{11}{section.3.1}%
\contentsline {section}{\numberline {3.2}Evaluating Character Understanding of Large Language Models via Character Profiling from Fictional Works}{12}{section.3.2}%
\contentsline {section}{\numberline {3.3}Project Gutenberg}{12}{section.3.3}%
\contentsline {section}{\numberline {3.4}LISCU}{13}{section.3.4}%
\contentsline {section}{\numberline {3.5}Dated Data: Tracing Knowledge Cutoffs in Large Language Models}{13}{section.3.5}%
\contentsline {section}{\numberline {3.6}Methodological Approach}{14}{section.3.6}%
\contentsline {chapter}{\numberline {4}Gathering of Literature}{17}{chapter.4}%
\contentsline {section}{\numberline {4.1}Querying Wikidata}{19}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Scraping Fandom Articles}{20}{subsection.4.1.1}%
\contentsline {chapter}{\numberline {5}Experiments}{23}{chapter.5}%
\contentsline {section}{\numberline {5.1}Baseline Experiment}{23}{section.5.1}%
\contentsline {subsection}{\numberline {5.1.1}Results}{26}{subsection.5.1.1}%
\contentsline {subsection}{\numberline {5.1.2}Analysis}{31}{subsection.5.1.2}%
\contentsline {section}{\numberline {5.2}Passage Embedding Experiment}{33}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Results}{34}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Analysis}{35}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Method Comparison}{38}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}Results}{38}{subsection.5.3.1}%
\contentsline {subsection}{\numberline {5.3.2}Analysis}{40}{subsection.5.3.2}%
\contentsline {chapter}{\numberline {6}Conclusion}{45}{chapter.6}%
\contentsline {section}{\numberline {6.1}Summary}{45}{section.6.1}%
\contentsline {section}{\numberline {6.2}Future Work}{45}{section.6.2}%
\contentsline {section}{\numberline {6.3}Acknowledgments}{47}{section.6.3}%
\contentsline {chapter}{\numberline {7}Appendix}{49}{chapter.7}%
\contentsline {chapter}{\nonumberline Bibliography}{51}{chapter*.32}%
\contentsline {chapter}{Affidavit}{55}{chapter*.34}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
