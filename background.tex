\chapter{Background}

The background section explains basic NLP methods in order to gain important knowledge on which I can build as we continue to explore.

\section{Tokenization}
Tokens are the fundamental units of data processing in natural language processing (NLP). A token is the smallest meaningful unit of text, which can be a word, subword, or even a single character or punctuation mark. Tokenization is typically performed at one of three levels: single characters (character-based tokenization), subwords (subword-based tokenization), or whole words (word-based tokenization).

In most modern NLP models, subword tokenization is predominantly used. This technique breaks words into smaller units, such as prefixes and suffixes. Unlike word-based tokenizers, which generate a very large vocabulary and suffer from a loss of meaning across very similar words as well as a large quantity of out-of-vocabulary tokens, or character-based tokenization, where each token has minimal meaning in context and the overall number of tokens on a tokenized text is enormous, subword-based tokenization seeks to find a middle ground. The idea is to decompose rare words into meaningful subwords while maintaining few to single tokens for every meaningful or frequently used word.

Byte Pair Encoding (BPE) is a simple and popular method for subword tokenization.
BPE describes an algorithm of frequency-based merging of character pairs into subwords. It starts out with single character symbols and iteratively creates new candidates by merging the most frequent pairs of adjacent symbols until a specified vocabulary size is reached.
Another popular tokenizer is the WordPiece tokenizer. The WordPiece tokenizer is quiet similar, but uses a different merging criteria. Instead of choosing the most frequent pairs, it chooses pairs that maximize the likelihood of the training corpus by dividing the frequency of the pair by the product of frequency of the individual tokens.

Now both of these methods require a pre-tokenized text or whitespace boundaries. However, not all languages use spaces to separate words such as in chinese or japanese. A more general approach than using specific pre-tokenizers is SentencePiece tokenization, that treats the input as a raw input stream, thus including the space in the set of characters to use. It then uses BPE or unigram tokenization, a probabilistic method that selects the best segmentation of text based on the likelihood of subwords to make a purely end-to-end and language independent system.

Subword tokenizers are employed in almost every widely used large language model such as GPT-2, Llama 3, and in large pre-trained language models such as BERT.\cite{huggingfacetokenizer}


\section{The Transformer}
The Transformer architecture, introduced in June 2017 \cite{NIPS2017_3f5ee243}, marked a significant advancement in natural language processing (NLP), initially focusing on sequence-to-sequence NLP problems like machine translation tasks. However, its capabilities quickly revealed a broader potential, particularly in developing large language models (LLMs). These models are trained on vast amounts of raw text using self-supervised learning, a method where the training objective is derived automatically from the input data, typically through tasks such as masked language modeling or causal language modeling. These techniques help the model develop a statistical understanding of the language. The Transformer architecture consists of an encoder and a decoder.

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{ressources/images/Transformer.png}
    \caption{Transformer Architecture as presented in the original paper [Vas+17]. This diagram illustrates the various layers of the architecture within the Encoder and Decoder, demonstrating the flow of data through each component.}
    \label{fig:transformer}
    \end{figure}

\subsection{Encoder}
For each token an embedding vector is computed, which is a very high-dimensional (currently at around 256 to 4096 dimensions) numerical vector representation of that token, that captures its semantic meaning.

A key component of the encoder is the self-attention mechanism. Self-attention enables the model to consider the entire sequence when encoding each token, allowing it to weigh the relevance of other tokens in the input sequence dynamically. For each token, the self-attention mechanism computes attention scores that determine the influence of all other tokens in the sequence. As a result, the generated embedded vector for each token not only represents the token alone but also its left and right contextual influence.
The encoder consists of multiple identical layers, or encoder blocks. Each encoder block contains two main sub-layers:

\begin{itemize}
    \item \textbf{Multi-Head Self-Attention Layer}: This sub-layer allows the model to attend to different parts of the sequence from multiple perspectives or ``heads''. Each head performs self-attention independently, and their outputs are concatenated and linearly transformed to provide a richer representation.

    \item \textbf{Feed-Forward Layer}: After the self-attention sub-layer, each token's representation is passed through a feed-forward neural network. This layer is a simple fully connected feed-forward network applied to each position (word) in the sequence independently and identically. It consists of two linear transformations with a non-liniarity (e.g. ReLU) activation in between, allowing the model to apply non-linear transformations and further refine the encoded representation.
\end{itemize}

Both sub-layers in the encoder block are followed by residual connections and layer normalization, which help in stabilizing the training and improving convergence.

\subsection{Decoder}
The decoder has a similar structur and therfore works quiet as similar as the encoder and can also be used for similar tasks. The decoder uses multiple decoder blocks, but has two additional sub-layers per block as compared to the encoder block. In the transformer's architecture the decoder's role is to generate the output sequence based on the encoded representation from the encoder (referred to ascross-attention). This is done auto-regressively, which means that the generated computed feature-vector, which holds information about the input sequence will be tranformed by the language modelling head mapping into the next probable following word, which then will be added to the input text and and is then fed back into the decoder. The most important difference to the encoder is the masked multi-head self-attention.

\begin{itemize}
    \item \textbf{Masked Multi-Head Self-Attention Layer}:
          Since the decoder cannot predict future words based on information not yet generated, it only attends uni-directional to the previously generated tokens in the output sequence. Therfore only the left context is used and the right context is masked. Masking the right context is of course only applicable for left-to-right written languages e.g. languages in european and american countries that utilize the latin script.
\end{itemize}
\cite{raschka2024encoderdecoder, NIPS2017_3f5ee243}

\section{BERT}
BERT (Bidirectional Encoder Representations from Transformers) is a pre-trained language representation model introduced by Devlin et al. in 2019 (\cite{bert-score}). Its based on the Transformer architecture from \cite{NIPS2017_3f5ee243} but instead of using in contrast to using both, an encoder and a decoder as in the original transformer, BERT only utilizes the encoder component. Consequently, unlike other large language models (LLMs), BERT cannot predict new tokens and thus is not suitable for text generation. Instead, it achieved state-of-the-art results in tasks such as text classification, sentiment analysis, and named entity recognition. The attention scores are computed using queries, keys, and values derived from the input embeddings.

\subsection{Embeddings}
The three matrices in BERT—token embeddings, segment embeddings, and positional embeddings are generated as part of the model's training process.

For each unique Token ID (i.e. for each of the 30,522 words and subwords in the BERT Tokenizer’s vocabulary), the BERT model contains an embedding that is trained to represent that specific token. The Embedding Layer within the model is responsible for mapping tokens to their corresponding embeddings.
Before a string of text is passed to the BERT model, the BERT Tokenizer is used to convert the input from a string into a list of integer Token IDs, where each ID directly maps to a word or part of a word in the original string. In addition to the Token Embeddings described so far, BERT also relies on Position Embeddings. While Token Embeddings are used to represent each possible word or subword that can be provided to the model, Position Embeddings represent the position of each token in the input sequence.
The final type of embedding used by BERT is the Token Type Embedding, also called the Segment Embedding in the original BERT paper. One of the tasks that BERT was originally trained to solve was Next Sentence Prediction. That is, given two sentences A and B, BERT was trained to determine whether B logically follows A.\\

BERT introduces two pre-training objectives, the masked language model objective (MLM), and the next sentence prediction objective (NSP).


\begin{itemize}
    \item \textbf{Masked Language Modeling (MLM)}:
          15\% of the words in a sentence are randomly masked, and the model is trained to predict these masked words based on the context provided by the other words in the sentence. This enables BERT to learn bidirectional representations.

    \item \textbf{Next Sentence Prediction (NSP)}:
          To understand relationships between sentences, BERT is trained on pairs of sentences. Given two sentences, the model predicts whether the second sentence is the actual next sentence in the original text or a randomly chosen one. This task helps BERT capture the coherence and context between sentences.
\end{itemize}


\section{Fine-Tuning}
After pre-training on large text corpora, BERT can be fine-tuned for specific downstream tasks using relatively small datasets. Fine-tuning involves making slight adjustments to the pre-trained model weights to better align with the target task. This process leverages robust pre-trained language representations and adapts them to meet the specific requirements of the task at hand.

One can distiguish between ``Feature extraction'' and ``Full fine-tuning'' as different types for fine-tuning. In Full fine-tuning, the entire model is trained on new task-specific data, meaning all model layers are adjusted during this process. This method is particularly advantageous when the task-specific dataset is large and significantly different from the pre-training data. Feature extraction treats the pre-trained model as a fixed feature extractor. Only the final layers of the model (or even newly added layers) are then trained on the task-specific data, while the rest of the model remains unchanged. It therfore is a more cost-effective and efficient way to fine-tune pre-trained language models.

Fine-tuning closely resembles regular training but, as mentioned, may only affect certain weights, depending on the chosen method. During fine-tuning, the loss is calculated based on the difference between the model's predictions and the true labels using backpropagation. Gradients of the loss are then computed and used to update the model's weights through gradient descent, iteratively refining the model to minimize the loss and enhance its performance. \cite{finetune}




\section{Quantization}
Quantization is a technique designed to reduce the computational and memory demands of running inference using low-precision data types, such as 8-bit integers (int8), instead of the standard 32-bit floating-point (float32). This approach decreases memory storage requirements, theoretically lowers energy consumption, and accelerates operations like matrix multiplication through integer arithmetic. Moreover, it enables models to operate on embedded devices that may only support integer data types.

\subsection{BERTScore}
BERTScore is an evaluation metric that utilizes the BERT model to compare texts more semantically than traditional metrics like BLEU. It leverages the contextualized embeddings provided by a pre-trained BERT model to assess the similarity between candidate and reference texts.\\

The process begins by inputting both candidate and reference texts into the BERT-style model, which generates contextualized embeddings for each token in both texts. For each token, the similarity between its embedding and every token embedding in the comparison text is calculated using cosine similarity
\begin{equation}
    \cos(\theta) = \frac{\mathbf{A} \cdot \mathbf{B}}{\|\mathbf{A}\| \|\mathbf{B}\|} = \frac{\sum_{i=1}^{n} \mathbf{A}_{i} \mathbf{B}_{i} }{\sqrt{\sum_{i=1}^{n} \mathbf{A}_{i}} \cdot \sqrt{\sum_{i=1}^{n} \mathbf{B}_{i}} }
\end{equation}
This results in a similarity matrix where each entry represents the cosine similarity between the embeddings of a pair of tokens (one from the candidate sentence and one from the reference sentence).\\


The metric is computed symmetrically as follows:\\
For each token embedding in the candidate sentence, find the maximum similarity score with any token embedding in the reference sentence, and average these scores across all tokens in the candidate sentence to obtain precision.\\

Similarly, for each token embedding in the reference sentence, find the maximum similarity score with any token embedding in the candidate sentence, and average these scores across all tokens in the reference sentence to obtain recall.

\[P_{BERT} = \frac{1}{|\hat{x}|} \sum_{\hat{x}_j\in \hat{x}} \max_{x_i \in x} x_i^T \hat{x_j} \]
\[R_{BERT} = \frac{1}{|x|} \sum_{x_i \in x} \max_{\hat{x}_j\in \hat{x}} x_i^T \hat{x_j} \]


Finally, the $F_1$-score (an $F$-measure) is computed as the harmonic mean of precision and recall, providing a balanced measure that considers both the model's ability to capture relevant information and its accuracy in predicting new text equally. This works particularly well in BERTScore because the individual tokens are represented as contextualized embeddings, which means that when comparing two token embeddings and receiving similar results does not only generally mean that the tokens are quite similar semantically but also in regards to their positional context of the entire sentence.


\[F_{BERT} = 2\frac{P_{BERT}R_{BERT}}{P_{BERT} + R_{BERT}} \]\cite{bert-score}

\section{BLEU-Score}

BLEU-Score is a different metric I use in my thesis for comparing texts. BLEU does not evaluate and compare the semantics of the reference and candidate text but instead compares the similarity of vocabulary between them.

Let $\left\{y^{1}, y^{2}, ..., y^{N}\right\}$ be the words of the reference text and $\left\{\hat{y}^{1}, \hat{y}^{2}, ..., \hat{y}^{N}\right\}$

The first step is to create n-grams $\text{G}_n(y)$ for both texts. An n-gram is just a set of consecutive words of length n in a text.

\[
    \text{G}_n(y) = \left\{y_1, y_2, ..., y_k\right\}
\]

Next, we define the function $\text{C}(s,y)$ that counts the appearances of s as a substring in y.
Now we can count n-grams of the candidate that appear in the reference text. We can compute the clipped precision by taking the minimum of the appearances of the n-gram in $y$ and $\hat{y}$ and then dividing by the amount of all occurrences of n-grams in $\hat{y}$. Therefore candidates that have the same n-gram repeating over and over again don't get a higher precision score if the same n-gram does not appear in the reference text the same amount.

\[
    \text{p}_n(\hat{y} , y) = \frac{\sum_{s \in G_n(\hat{y})} \min(\text{C}(s,\hat{y}), \text{C}(s,y))}{\sum_{s \in G_n(\hat{y})} \text{C}(s,\hat{y})}
\]


Right now short candidate texts are more likely to get a good score although the reference text is much longer. Therefore we add a brevity penalty in order to give higher scores to texts that are closer or even longer to the reference texts' real size.
\[
    \text{BP}(c, r) = \left\{\begin{array}{lr}
        1,               & \text{if } c > r    \\
        \ e^{(1 - r/c)}, & \text{if } c \leq r \\
    \end{array}\right\}
\]

Finally, for BLEU-Score we combine the brevity penalty with the clipped precision of n-grams. We additionally add a distribution vector to weigh each $ \text{p}_n$ by $w_n$ in order to have the opportunity to give n-grams with different $n$ also a different impact on the overall result. Although in the end most BLEU-Scores just use a uniform distribution with $N = 4$ so that $w_n$ always stays $\frac{1}{4}$


\[\text{BLEU} = \text{BP}(c, r) \cdot \exp\left(\sum_{n=1}^{N}  \text{w}_n \cdot \ln(p_n)\right)\]\cite{doshi2021foundations,suvratarora2024nlp}



\section{RAG}
Retrieval-augmented generation (RAG), is a technique used to improve the quality of LLM-generated responses by grounding the model on external sources, by passing the sources along with the prompt as input. LLMs are inconsistent in terms of producing same quality responses for each and every topic, since they knowledge is based on finite amount of information, that is not equally distributed for every potential topic. But Retrieval-augmented generation doesn't only reduce the need for internal sources (continuous training, lowering computational and financial costs) but also ensures that the model has access to the most current, reliable facts. It therefore reduces the chances of generating false information and ensures that the generated content is relevant.
In this thesis, I am utilizing RAG to hopefully improve (key features) from the characters described in the literature to achieve better characterizations with grounded models that utilize this external information.\cite{aws_rag}
